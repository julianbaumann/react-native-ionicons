# Ionicons for React Native

This library uses the open sourced [Ionicons](https://ionicons.com) `v5`.

Should work and look exacly like it would on the ionic-framework. If not, feel free to create a new issue.

>I know there are good other libraries out there which do exacly the same, but I encountered some issues like coloring, or they use old v4/v3 icons. Plus with this library you can use StyleSheets to style the icon (color and fontSize).
As long as ionic doesn't change the way the icons work or remove the "Designer pack" download, this library should be easy to maintain, since there is only one script to run to generate the icons.

## Installation
> This library uses `react-native-svg` to display the icons, so we must first install that dependency.

> [Installation guide from react-native-svg](https://www.npmjs.com/package/react-native-svg#installation) (for expo etc)

React Native CLI:

```bash
$ npm install react-native-svg --save
```

Link native code (on a mac): 
```bash
$ cd ios && pod install
```

<br />

Now install this package
```bash
$ npm install @julianbaumann/react-native-ionicons@latest --save
```

## Usage
```ts
import { Ionicon } from "@julianbaumann/react-native-ionicons";

...

const styles = {
    Icon: {
        fontSize: 30,
        color: "red"
    }
}

return (
    <Ionicon name={"code-slash"} style={styles.Icon} />

    <Ionicon name={"code-slash"} size={30} color={"red"} />

    <Ionicon ios={"code-slash-outline"} android={"code-slash"} />
)

```