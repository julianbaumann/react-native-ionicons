import * as React from "react";
interface IoniconProps {
    name?: string;
    color?: string;
    size?: number;
    style?: any;
    ios?: string;
    android?: string;
}
export declare const Ionicon: React.FunctionComponent<IoniconProps>;
export {};
