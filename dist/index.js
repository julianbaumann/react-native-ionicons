"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Ionicon = void 0;
const React = __importStar(require("react"));
const react_native_1 = require("react-native");
const react_native_svg_1 = require("react-native-svg");
const icons_1 = require("./icons");
exports.Ionicon = (props) => {
    if (!props?.name && !props?.ios && !props?.android) {
        console.error("Please set either 'name', 'ios' or 'android' as icon source");
        return null;
    }
    let name = props?.name;
    if (react_native_1.Platform.OS == "ios" && props?.ios) {
        name = props?.ios;
    }
    else if (react_native_1.Platform.OS == "android" && props?.android) {
        name = props?.android;
    }
    else if (react_native_1.Platform.OS == "ios" && !props?.ios && !props?.name) {
        return null;
    }
    else if (react_native_1.Platform.OS == "android" && !props?.android && !props?.name) {
        return null;
    }
    const symbol = icons_1.Icons.find((element) => element.Name == name);
    if (!symbol) {
        console.warn(`No ionicon found with this name: ${props.name}`);
        return null;
    }
    const content = symbol?.Content;
    const size = props?.size || props?.style?.fontSize || 20;
    const color = props?.color || props?.style?.color || "black";
    const viewStyle = props?.style;
    const styles = {
        color: color,
        fill: "currentColor"
    };
    return (<react_native_1.View style={viewStyle}>
            <react_native_svg_1.SvgCss width={size} height={size} style={styles} xml={content}/>
        </react_native_1.View>);
};
