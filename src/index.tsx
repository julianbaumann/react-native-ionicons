import * as React from "react";
import { Platform, PlatformColor, View } from "react-native";
import { SvgCss } from "react-native-svg";

import { Icons } from "./icons";

interface IoniconProps
{
    name?: string;
    color?: string;
    size?: number;
    style?: any;
    ios?: string;
    android?: string;
}

interface Icon
{
    Name: string;
    Content: string;
}

export const Ionicon: React.FunctionComponent<IoniconProps> = (props: IoniconProps): JSX.Element =>
{
    if (!props?.name && !props?.ios && !props?.android)
    {
        console.error("Please set either 'name', 'ios' or 'android' as icon source");
        return null;
    }

    let name: string = props?.name;

    if (Platform.OS == "ios" && props?.ios)
    {
        name = props?.ios;
    }
    else if (Platform.OS == "android" && props?.android)
    {
        name = props?.android;
    }
    else if (Platform.OS == "ios" && !props?.ios && !props?.name)
    {
        return null;
    }
    else if (Platform.OS == "android" && !props?.android && !props?.name)
    {
        return null;
    }

    const symbol: Icon = Icons.find((element: Icon) => element.Name == name);

    if (!symbol)
    {
        console.warn(`No ionicon found with this name: ${props.name}`);
        return null;
    }

    const content: string = symbol?.Content;

    const size: number = props?.size || props?.style?.fontSize || 20;
    const color: any = props?.color || props?.style?.color || (Platform.OS == "ios") ? PlatformColor("label") : "black";

    const viewStyle: any = props?.style;

    const styles: any = {
        color: color,
        fill: "currentColor"
    };

    return (
        <View style={viewStyle}>
            <SvgCss width={size} height={size} style={styles} xml={content} />
        </View>
    );

};