#!/bin/bash

# Colors
RedColor="\\033[0;31m"
DefaultColor="\\033[0m"
GreenColor="\\033[0;32m"
BlueColor="\\033[0;34m"

# Convert icons
JsonContent="export const Icons = ["

for file in icons/*
do
    BaseNameOfFile="$(basename "$file")";
    NameWithoutExtension=$(echo "${BaseNameOfFile//.svg}");
    FileContentRaw=$(cat "$file");
    FileContent=$(echo ${FileContentRaw//\"/\\\\\"});
    JsonContent="${JsonContent}\n    {\"Name\": \"${NameWithoutExtension}\", \"Content\": \""$FileContent"\" },";
    echo -e "${BlueColor} $NameWithoutExtension ${DefaultColor}";
done
printf "${JsonContent::${#JsonContent} - 1}\n];" > src/icons.js;

echo -e "\n${GreenColor}Done${DefaultColor}";